<!-- plantilla principal -->

<!DOCTYPE html>
<html>
<head>
	<title>
		@yield('title') 
	</title>
</head>
<body>

	@include('layouts.nav')<!-- barra de navegación -->
	
	@yield('content')
</body>
</html>