@extends('layouts/layout')

@section('title', 'Titulos') <!-- titulo de la de la pestaña -->

@section('content')

	<!-- formulario para crear nuevos programas. Los datos se envían a la misma ruta y los toma la public function store del controllador (titlesController) para crear nuevos registros en la tabla faculties-->
	<form method="post" action= " {{route('titulos.store')}} " >
		{{csrf_field()}} 
		<input type="text" name="Name"><br>
		<select name="program_id">
			@foreach($programs as $program)
				<option value="{{ $program->id }}">{{ $program->Name }}</option>
			@endforeach
  		</select>
		<textarea name="Description" ></textarea>
		
		<input type="submit" name="send" value="Enviar">
	</form>

	<!-- este foreach muestra los datos de la tabla faculties--> 
	<nav>
		@foreach($titles as $title)
			<ul><a href="/titulos/{{ $title->id }}">{{$title->Name}}</a> name</ul>
		@endforeach
	</nav>
	
@endsection