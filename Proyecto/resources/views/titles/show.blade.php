@extends('..layouts/layout')

@section('title', 'Titulos') <!-- titulo de la de la pestaña -->

@section('content')
	
	<h1>Editar</h1>

	<!-- formulario para enviar datos actualizados de un titulo ya existente. Los datos se envían a la misma ruta y los toma la public function edit del controllador (titlesController), para actualizar  registros en la tabla titles-->
	<form method="post" action= "/titulos/{{$title->id}}" >
		{{csrf_field()}}
		<input type="hidden" name="_method" value="PUT">
		<input type="text" name="Name" value="{{$title->Name}}">
		<br>
		<select name="program_id">
			@foreach($programs as $program)
				<option value="{{ $program->id }}">{{ $program->Name }}</option>
			@endforeach
  		</select>
  		<br>
		<textarea name="Description" ></textarea>
		<input type="submit" name="send" value="Enviar">
	</form>

	<!-- Boton para enviar el id del titulo y eliminarlo desde public function edit en el controllador (titlesController), de la tabla titles --> 
	<form method="post" action= "/titulos/{{$title->id}}" >
		{{csrf_field()}}
		<input type="hidden" name="_method" value="DELETE">
		<input type="submit" name="send" value="Eliminar">
	</form>

@endsection