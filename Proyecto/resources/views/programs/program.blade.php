@extends('layouts/layout')

@section('title', 'Programas') <!-- titulo de la de la pestaña -->

@section('content')

	<!-- formulario para crear nuevos programas. Los datos se envían a la misma ruta y los toma la public function store del controllador (programsController) para crear nuevos registros en la tabla faculties-->
	<form method="post" action= " {{route('programas.store')}} " >
		{{csrf_field()}} 
		<input type="text" name="Name"><br>
		<select name="faculty_id">
			@foreach($faculties as $faculty)
				<option value="{{ $faculty->id }}">{{ $faculty->Name }}</option>
			@endforeach
  		</select>
		<textarea name="Description" ></textarea>
		
		<input type="submit" name="send" value="Enviar">
	</form>

	<!-- este foreach muestra los datos de la tabla faculties--> 
	<nav>
		@foreach($programs as $program)
			<ul><a href="/programas/{{ $program->id }}">{{$program->Name}}</a> name</ul>
		@endforeach
	</nav>
	
@endsection