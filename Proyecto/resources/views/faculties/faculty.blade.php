<!-- vista principal de facultades, incluida desde la public function index en el controllador facultiesController-->

@extends('layouts/layout')

@section('title', 'Facultades') <!-- titulo de la de la pestaña -->

@section('content')

	<!-- formulario para crear nuevas facultades. Los datos se envían a la misma ruta y los toma la public function store del controllador (facultiesController) para crear nuevos registros en la tabla faculties-->
	<form method="post" action= " {{route('facultades.store')}} " >
		{{csrf_field()}} 
		<input type="text" name="Name"><br>
		<textarea name="Description" ></textarea>
		<input type="submit" name="send" value="Enviar">
	</form>

	<!-- este foreach muestra los datos de la tabla faculties--> 
	<nav> 
		<li>
		@foreach ($faculties as $faculty)
    		<ul><a href="/facultades/{{$faculty->id}}">{{$faculty->Name}}</a> name</ul> 
    		<ul>{{$faculty->Description}} descipcion</ul> 
		@endforeach
		</li> 
	</nav>
	
@endsection