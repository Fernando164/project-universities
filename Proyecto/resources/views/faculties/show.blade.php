@extends('..layouts/layout')

@section('title', 'Modificar facultades') <!-- titulo de la de la pestaña -->

@section('content')
	
	<h1>Editar</h1>

	<!-- formulario para enviar datos actualizados de una facultad ya existente. Los datos se envían a la misma ruta y los toma la public function edit del controllador (facultiesController), para actualizar  registros en la tabla faculties-->
	<form method="post" action= "/facultades/{{$faculty->id}}" >
		{{csrf_field()}}
		<input type="hidden" name="_method" value="PUT">
		<input type="text" name="Name" value="{{$faculty->Name}}"><br>
		<textarea name="Description" ></textarea>
		<input type="submit" name="send" value="Enviar">
	</form>

	<!-- Boton para enviar el id de la facultad y eliminarla desde public function edit en el controllador (facultiesController), de la tabla faculties --> 
	<form method="post" action= "/facultades/{{$faculty->id}}" >
		{{csrf_field()}}
		<input type="hidden" name="_method" value="DELETE">
		<input type="submit" name="send" value="Eliminar">
	</form>

@endsection