<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    
    protected $fillable=[
    	"Name",
    	"Description"

    ];

    public function program(){

    	return $this->hasOne("App\Program");

    }

    public function programs(){

    	return $this->hasMany("App\Program");

    }
    

}
