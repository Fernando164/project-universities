<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Title;
use App\Program;

class titlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titles = Title::all();
        $programs = Program::all();

        return view("titles/title", compact('titles', 'programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['Name' => 'required']);
        /*Recibe los datos del formulario en la ruta titulos (archivo -> title.blade.php) para crear datos en la tabla -> titles*/ 
        $Title = new Title;
        $Title->Name = $request->Name;
        $Title->program_id = $request->program_id;
        $Title->Description = $request->Description;
        $Title->save();

        return redirect('/titulos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = Title::find($id);
        $programs = Program::all();

        //vista 
        return view('titles/show', compact('title', 'programs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = Title::find($id);

        $title->update($request->all());

        return redirect("/titulos/{$id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $title = Title::find($id);

        $title->delete();

        return redirect("/titulos"); 
    }
}
