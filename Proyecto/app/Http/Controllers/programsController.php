<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Faculty;

class programsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $programs = Program::all();
        $faculties = Faculty::all();

        return view("programs/program", compact('programs', 'faculties'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "404";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*Recibe los datos del formulario en la ruta programas (archivo -> programs.blade.php) para crear datos en la tabla -> faculties*/ 
        $program = new Program;
        $program->Name = $request->Name;
        $program->faculty_id = $request->faculty_id;
        $program->Description = $request->Description;
        $program->save();

        return redirect('/programas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = Program::find($id);
        $faculties = Faculty::all();

        //vista 
        return view('programs/show', compact('program', 'faculties'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = Program::find($id);

        $program->update($request->all());

        return redirect("/programas/{$id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);

        $program->delete();

        return redirect("/programas"); 
    }
}
