<?php

use App\Faculty;
use App\Program;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*ruta para la pagina de inicio"*/
/*Route::get('/', function(){
	$user = Auth::user();

	return $user;
});*/


Route::resource('/facultades', 'facultiesController');
Route::resource('/programas', 'programsController');
Route::resource('/titulos', 'titlesController');




Route::post('facultades/store', 'facultiesController@store')->name('facultades.store');






/* uno a uno id de programa a facultadad*/
Route::get('/programas/{id}/facultad', function($id){
	return Program::find($id)->faculty->Name;
});

/*uno a uno id de facultad a programa*/ 
Route::get('/facultad/{id}/programas', function($id){
	return Faculty::find($id)->program->Name;


});


/*uno a varios (de la id de faculytadad a sus respectivos programas)*/ 
Route::get('/programas_f', function(){
	return $programs = Faculty::find(1)->programs;

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
